
#include <iostream>
#include <filesystem>
#include <fstream> // подключаем файлы
#include <string>

namespace fs = std::filesystem;
using namespace std;

int main()
{
    std::string path = "./";
    for (const auto & entry : fs::directory_iterator(path)) {
        //cout << entry.path() << endl;
        string path2 = entry.path();
       // cout << path2 << endl;
        std::ifstream file(path2);
        std::string s;
        std::getline(file, s);
        cout << s << endl;
    }
    return 0;
}

